#!/bin/bash

VERSION=$(awk -F"=" '{print $2}' /etc/lsb-release | sed -n 2p)

if ! which nodejs >/dev/null 
then
    echo "********** Instalar nodejs **********"
    curl -sL https://deb.nodesource.com/setup_4.x | sudo -E bash -
    sudo apt-get install -y nodejs
    echo -n "Node version: "; node -v
fi


if ! which mongo >/dev/null 
then
    echo "********** Instalar mongodb **********"
    sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927

    if [ $VERSION = "16.04" ]
    then 
            echo "deb http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list
    elif [ $VERSION = "14.04" ]
    then
            echo "deb http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list
    else
            echo "deb http://repo.mongodb.org/apt/ubuntu precise/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list
    fi

    sudo apt-get update
    sudo apt-get install -y mongodb-org

    mongo --version
fi

if ! which gulp >/dev/null 
then
    echo "********** Instalar gulp **********"
    sudo npm install -g gulp
    gulp -v
fi

if ! which bower >/dev/null 
then
    echo "********** Instalar bower **********"
    sudo npm install -g bower
    echo -n "Bower version: "; bower -v
fi

if ! which git >/dev/null 
then
    echo "********** Instalar git **********"
    sudo apt-get install git
    git --version
fi

# Lista todas as versões do git | sort no resultado das versões | pega as duas ultimas linhas | pega a antepenultima linha | pega a versão
NPM_LAST_VERSION=$(git ls-remote --tags https://github.com/npm/npm.git | sort -t '/' -k 3 -V | tail -2 | head -1 | awk -F"/v" '{print $2}')
echo $NPM_LAST_VERSION

if [ $(npm -v) != $NPM_LAST_VERSION ]
then
    echo "********** Updating npm **********"
    echo última versão no git $NPM_LAST_VERSION
    echo -n "versão instalada: "; npm -v
    sudo npm install npm -g
    echo -n "npm version: "; npm -v
fi
