#!/bin/bash

# Funtion to create DesktopEntry
# CreateDesktopEntry $programName \$comment $pathExec $pathIcon $category $version $entryName
CreateDesktopEntry () {
    eval name="$1"  # Ver: http://stackoverflow.com/questions/1983048/passing-a-string-with-spaces-as-a-function-argument-in-bash
    eval comment="$2"  # Ver: http://stackoverflow.com/questions/1983048/passing-a-string-with-spaces-as-a-function-argument-in-bash
    content="[Desktop Entry]\nName=$name\nComment=$comment\nExec=$3\nIcon=$4\nTerminal=false\nType=Application\nEncoding=UTF-8\nCategories=$5\nVersion=$6"
    echo -e $content | sudo tee /usr/share/applications/$7
}

Clean () {
    echo "*****Remover pacoter desnecessários*****"
    sudo aptitude clean
    sudo apt-get autoremove
    sudo aptitude autoclean
}

if ! which fluxgui >/dev/null 
then
	echo "Deseja instalar o f.lux GUI [S/n]"
	read down_eclipsePDT;
	if [ $down_eclipsePDT = "S" ];
	then
		sudo add-apt-repository ppa:nathan-renniewaldock/flux
 		sudo aptitude update 
		sudo aptitude install fluxgui
		sudo add-apt-repository -r ppa:nathan-renniewaldock/flux
		echo "Maiores detalher em: https://justgetflux.com/linux.html"	
		fluxgui
	fi
fi

if ! which skypeforlinux >/dev/null 
then
	echo "Deseja baixar o Skype for Linux Alpha [S/n]"
	read down_skype;
	if [ $down_skype = "S" ];
	then
        wget -c https://repo.skype.com/latest/skypeforlinux-64-alpha.deb
        sudo dpkg -i skypeforlinux-64-alpha.deb
		rm skypeforlinux-64-alpha.deb
	fi
fi
#if ! which skype >/dev/null 
#then
#	echo "Deseja baixar o skype [S/n]"
#	read down_skype;
#	if [ $down_skype = "S" ];
#	then
#		sudo apt-add-repository "deb http://archive.canonical.com/ubuntu xenial partner"
#		sudo aptitude update
#		sudo aptitude install skype
#		sudo add-apt-repository --remove "deb http://archive.canonical.com/ubuntu $(lsb_release -sc) partner"
#		echo 'Foi instalado a versão 4.3.0, conferir se há versão mais recente aqui:'
#		echo 'http://www.skype.com/en/download-skype/skype-for-linux/downloading/?type=ubuntu64'
#		sleep 3
#	fi
#fi

if ! which google-chrome >/dev/null 
then
	echo "Deseja baixar o chrome [S/n]"
	read down_chrome;
	if [ $down_chrome = "S" ];
	then
		sudo aptitude install libpango1.0-0 libpangox-1.0-0 
		wget -c https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
		sudo dpkg -i google-chrome-stable_current_amd64.deb
		rm google-chrome-stable_current_amd64.deb
		echo 'Foi instalado a versão mais recente a partir deste link:'
		echo 'https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb'
		sleep 3
	fi
fi

if ! which vivaldi >/dev/null 
then
	echo "Deseja baixar o vivaldi 1.3 [S/n]"
	read down_vivaldi;
	if [ $down_vivaldi = "S" ];
	then
		wget -c https://downloads.vivaldi.com/stable/vivaldi-stable_1.3.551.38-1_amd64.deb
		sudo dpkg -i vivaldi-stable_1.3.551.38-1_amd64.deb	
		echo 'Foi instalado a versão 1.3 a partir deste link:'
		echo 'https://vivaldi.com/download/?lang=pt_BR'
		sleep 3
		rm vivaldi-stable_1.3.551.38-1_amd64.deb
	fi
fi

if ! which opera >/dev/null 
then
	echo "Deseja baixar o opera 39 [S/n]"
	read down_opera;
	if [ $down_opera = "S" ];
	then
		wget -c http://download1.operacdn.com/pub/opera/desktop/39.0.2256.48/linux/opera-stable_39.0.2256.48_amd64.deb
		sudo dpkg -i opera-stable_39.0.2256.48_amd64.deb
		echo 'Foi instalado a versão 39 a partir deste link:'
		echo 'http://www.opera.com/pt-br/download'
		sleep 3
		rm opera-stable_39.0.2256.48_amd64.deb
	fi
fi

if ! which thunderbird >/dev/null 
then
	echo "Deseja baixar o thunderbird 45 [S/n]"
	read down_thunderbird;
	if [ $down_thunderbird = "S" ];
	then
		wget -c https://download-installer.cdn.mozilla.net/pub/thunderbird/releases/45.3.0/linux-x86_64/pt-BR/thunderbird-45.3.0.tar.bz2
		tar -jxvf thunderbird-45.3.0.tar.bz2
		sudo mv thunderbird/ /opt/
		sudo ln -s /opt/thunderbird/thunderbird /usr/bin/thunderbird
		sudo chmod 755 /opt/thunderbird/chrome/icons/default/default48.png
		programName='Thunderbird' comment="Thunderbird Email Messages" version='45.3' category='Email;Comunication;Application;'
		pathIcon='/opt/thunderbird/chrome/icons/default/default48.png' entryName='thunderbird.desktop' pathExec='/usr/bin/thunderbird'
		CreateDesktopEntry $programName \$comment $pathExec $pathIcon $category $version $entryName
		echo 'Foi instalado a versão 45 a partir deste link:'
		echo 'https://www.mozilla.org/pt-BR/thunderbird/'
		sleep 3
		rm thunderbird-45.3.0.tar.bz2
	fi
fi

if ! which zend-eclipse-php >/dev/null 
then
	echo "Deseja baixar o Zend Eclipse PDT 3.2.0 [S/n]"
	read down_eclipsePDT;
	if [ $down_eclipsePDT = "S" ];
	then
		wget -c http://downloads.zend.com/pdt/3.2.0/zend-eclipse-php-3.2.0-x86_64.tar.gz
		tar -xvf zend-eclipse-php-3.2.0-x86_64.tar.gz
		wget -c http://www.nodeclipse.org/img/icon-huge.png
		mv icon-huge.png eclipse-icon.png
		mv eclipse-icon.png zend-eclipse-php/
		sudo mv zend-eclipse-php/ /opt/
		sudo ln -s /opt/zend-eclipse-php/zend-eclipse-php /usr/bin/zend-eclipse-php
		sudo chmod 755 /opt/zend-eclipse-php/eclipse-icon.png

		programName="Zend Eclipse PDT" comment="IDE for Zend PHP" version='3.2.0' category='IDE;Eclipse;Application;Zend;PHP;'
		pathIcon='/opt/zend-eclipse-php/eclipse-icon.png' entryName='zend-eclipse-php.desktop' pathExec='/usr/bin/zend-eclipse-php'
		CreateDesktopEntry \$programName \$comment $pathExec $pathIcon $category $version $entryName
		echo 'Foi instalado a versão 3.2.0 a partir deste link:'
		echo 'http://downloads.zend.com/pdt/3.2.0/'
		sleep 3
		rm zend-eclipse-php-3.2.0-x86_64.tar.gz
	fi
fi

if ! which brackets >/dev/null 
then
	echo "Deseja instalar o Editor de Texto Brackets [S/n]"
	read down_brackets;
	if [ $down_brackets = "S" ];
	then
#		wget -c http://archive.ubuntu.com/ubuntu/pool/main/libg/libgcrypt11/libgcrypt11_1.5.3-2ubuntu4_i386.deb -O libgcrypt11.deb
#		sudo dpkg -i libgcrypt11.deb
#		sudo apt-get install -f
		wget -c http://archive.ubuntu.com/ubuntu/pool/main/libg/libgcrypt11/libgcrypt11_1.5.3-2ubuntu4_amd64.deb -O libgcrypt11.deb
		sudo dpkg -i libgcrypt11.deb
		sudo apt-get install -f
		wget -c https://github.com/adobe/brackets/releases/download/release-1.7/Brackets.Release.1.7.64-bit.deb
		sudo dpkg -i Brackets.Release.1.7.64-bit.deb
		rm libgcrypt11.deb 
		rm Brackets.Release.1.7.64-bit.deb
		echo "Plugins úteis:
			  Brackets Beautify
  			  Brackets Color Picker
			  BracketstoIX
  			  Custom Work
			  Dark Material Theme 2
			  Emmet
			  JSHint
			  Lintyai
			  LiveReload
			  Show Tabs
			  Simple To-Do"
	fi
fi

if ! ls /usr/local/ | grep netbeans >/dev/null 
then
	echo "Deseja instalar o NetBeans IDE [S/n]"
	read down_netbeans;
	if [ $down_netbeans = "S" ];
	then
		if ! which java >/dev/null 
		then
			sudo aptitude install openjdk-8-jdk
			sudo aptitude install openjdk-8-jre
			sudo update-alternatives --config java
		fi

		wget -c http://bits.netbeans.org/netbeans/8.1/community/bundles/netbeans-8.1-html-linux-x64.sh
		sudo sh netbeans-8.1-html-linux-x64.sh
		rm netbeans-8.1-html-linux-x64.sh
		echo "Baixe e instale o thema 'Darcula'"
		firefox --new-tab 'http://plugins.netbeans.org/plugin/62424/darcula-laf-for-netbeans'
	fi
fi

if ! which steam >/dev/null 
then
	read -p "Deseja instalar o Steam? [S/n]" -n 1 -r
	if [[ $REPLY =~ ^(S|s| ) ]]; then
		wget -c http://repo.steampowered.com/steam/archive/precise/steam_latest.deb
		dpkg -i steam_latest.deb
		rm steam_latest.deb
	fi
fi

Clean

echo "Instalar os seguinter programas: 
     Forticlient SSL-VPN ## Get packager here: https://hadler.me/linux/forticlient-sslvpn-deb-packages/
     Filezilla
     Firebug
     MySQL Workbench
     Netbeans
     PyCharm
     Format Junkie
     Wunderlist for Chrome"

