#!/bin/bash

if ! which aptitude >/dev/null 
then
    echo "********** Instalar aptitude **********"
    sudo apt-get install aptitude
fi

sudo aptitude update
echo "********** Instalar apache2 **********"
sudo aptitude install apache2
echo "********** Instalar mysql (server e client) **********"
sudo aptitude install mysql-server mysql-client
# sudo mysql_install_db  ## start mysql
# sudo /usr/bin/mysql_secure_installation
echo "********** Instalar php 7.0 + modulos **********"
sudo aptitude install php7.0 libapache2-mod-php7.0
sudo aptitude install php7.0-mcrypt php7.0-mysql php7.0-curl php7.0-dev php7.0-intl
# echo "Install php7.0-xdebug"
# cd ~/Downloads
# wget -c -O xdebug-2.4.1.tgz http://xdebug.org/files/xdebug-2.4.1.tgz
# tar -xvzf xdebug-2.4.1.tgz
# cd xdebug-2.4.1/
# phpize
# ./configure
# make
# sudo cp modules/xdebug.so /usr/lib/php/20151012

# sed usado para substituir string em arquivo
# sed -i -e 's/foo/bar/g' filename

#
#Instalar PHP + Apache
#	sudo apt-get update
#	sudo apt-get install php5 libapache2-mod-php5
#	
#Instalar pacotes do PHP
#
#	sudo apt-get install php5-mysql
#	sudo apt-get install php5-curl
#	sudo apt-get install php5-mcrypt
#	sudo apt-get install php5-memcached
#	sudo apt-get install php5-dev
#	sudo apt-get install php5-xdebug
#	sudo apt-get install php5-intl
#	
#Configurar o PHP
#
#	sudo nano /etc/php5/cli/php.ini  # /etc/php5/apache2/php.ini
#		From: display_errors = Off   
#		To:   display_errors = On   
#
#		From: ;default_charset = "UTF-8"
#		To:   default_charset = "UTF-8"
#
#		From: ;date.timezone = 	
#		To:   date.timezone = America/Sao_Paulo
#
#Mudar as permissões do diretório do Apache
#	
#	sudo chown -Rf ubuntu:ubuntu /var/www
#	
#Mudar projeto principal do apache
#
#	sudo nano /etc/apache2/sites-enabled/000-default.conf 
#		Search: 'DocumentRoot /var/www/html'
#		And change to: 'DocumentRoot /var/www/my-project/public'
#
#	sudo nano /etc/apache2/sites-available/default-ssl.conf
#		Search: 'DocumentRoot /var/www/html'
#		And change to: 'DocumentRoot /var/www/my-project/public'
#
#	sudo service apache2 restart
#
#Habilitar modo de reescrita + outros
#
#	apachectl -t -D DUMP_MODULES  #list your current modules
#        sudo a2enmod headers 		  #enable headers
#	sudo a2enmod expires  		  #enable expires
#	sudo a2enmod rewrite		  	#enable rewrite
#	sudo service apache2 restart
#
#Implementar controle de cache no Apache:
#
#	sudo nano /etc/apache2/apache2.conf  #edite este arquivo e adicione as linhas abaixo
#
#		#
#		# Implementacao do controle de cache no servidor
#		#
#
#		# BEGIN Expire headers
#		ExpiresActive On
#		ExpiresDefault "access plus 0 seconds"
#		# END Expire headers
#
#		# BEGIN Cache-Control Headers
#		<FilesMatch "\.(x?html?|php)$">
#			Header set Cache-Control "max-age=0, private, no-store, no-cache, must-revalidate"
#		</FilesMatch>
#		<filesMatch ".(ico|jpe?g|png|gif|swf)$">
#			Header set Cache-Control "public"
#		</filesMatch>
#		<filesMatch ".(js|css)$">
#			Header set Cache-Control "private"
#		</filesMatch>
#		# END Cache-Control Headers
#
#		# BEGIN Turn ETags Off
#		FileETag None
#		# END Turn ETags Off
#
#
#		##########----##########
#		From:  
#			<Directory /var/www/>  
#			   Options Indexes FollowSymLinks  
#			   AllowOverride None          #<--Here  
#			   Require all granted  
#		    </Directory>  
#	    To:  
#		    <Directory /var/www/>  
#		    	Options Indexes FollowSymLinks  
#			    AllowOverride FileInfo     #<--Here  
#			    Require all granted  
#		    </Directory>  
#
#Resolvendo a mensagem 'AH00558: apache2: Could not reliably determine the server's fully qualified #domain #name, using 127.0.0.1. Set the 'ServerName' directive globally to suppress this message'
#	echo "ServerName localhost" | sudo tee /etc/apache2/conf-available/servername.conf
#	sudo a2enconf servername
#    sudo service apache2 restart 
#
#Instalar MySQL (client and server).
#        
#	sudo apt-get install mysql-client    #and press Y and ENTER when prompted.
#	sudo apt-get install mysql-server    #and press Y and ENTER when prompted.
#	sudo apt-get update   				 #update the repositories status.	

