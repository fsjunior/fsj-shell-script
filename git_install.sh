#!/bin/bash

sudo apt-get update
sudo apt-get install git

echo "Digite o nome do usuário. Ex: Chuck Norris"
read nome
git config --global user.name "$nome"

echo "Digite o email. Ex: chuck@norris.com"
read email
git config --global user.email $email

# Ver os itens de configuração do git
git config --list
