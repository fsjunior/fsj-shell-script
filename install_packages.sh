#!/bin/bash

Clean () {
    echo "*****Remover pacoter desnecessários*****"
    sudo aptitude clean
    sudo apt-get autoremove
    sudo aptitude autoclean
}

echo "*****Instalar o aptitude*****"
sudo apt-get install aptitude
echo "*****Atualizar repositórios - update*****"
sudo aptitude update
echo "*****Atualizar pacotes/programas - upgrade*****"
sudo aptitude upgrade
echo "*****Instalar terminator*****"
sudo aptitude install terminator
echo "*****Instalar o htop******"
sudo aptitude install htop
echo "*****Instalar unZIP******"
sudo aptitude install unzip
echo "*****Instalar unRAR******"
sudo aptitude install unrar
echo "*****Instalar o Wine******"
sudo aptitude install wine
echo "*****Instalar o pngquant*****"
sudo aptitude install pngquant
echo "*****Instalar o player MPV*****"
sudo aptitude install mpv
echo "*****Instalar o player SMPlayer*****"
sudo aptitude install smplayer
echo "*****Instalar o audacious*****"
sudo aptitude install audacious
echo "*****Instalar cliente torrent - Deluge*****"
sudo aptitude install deluge
echo "*****Instalar cliente torrent - qBittorrent*****"
sudo aptitude install bittorrent
echo "*****Instalar programa para baixar vídeor - youtube-dl*****"
sudo aptitude install youtube-dl
echo "*****Instalar java jdk e jre*****"
sudo aptitude install openjdk-8-jdk
sudo aptitude install openjdk-8-jre
sudo update-alternatives --config java
java -version
echo "*****Simple Screen Recorder*****"
sudo add-apt-repository ppa:maarten-baert/simplescreenrecorder && sudo apt-get update && sudo aptitude install simplescreenrecorder && sudo apt-get install simplescreenrecorder-lib:i386 && sudo add-apt-repository -r ppa:maarten-baert/simplescreenrecorder
echo "*****Pinta (Alternativa para o PAINT)*****"
# sudo add-apt-repository ppa:pinta-maintainers/pinta-stable && sudo apt-get update && sudo aptitude install pinta && sudo add-apt-repository -r ppa:pinta-maintainers/pinta-stable
sudo aptitude install pinta
echo "*****Notepadqq*****"
sudo add-apt-repository ppa:notepadqq-team/notepadqq && sudo apt-get update && sudo aptitude install notepadqq && sudo add-apt-repository -r ppa:notepadqq-team/notepadqq

echo "*****Instalar Nmap******"
sudo apt-get install nmap
echo "*****Instalar Traceroute******"
sudo apt-get install traceroute

echo "*****Instalar o Easter Egg da vaquinha******"
sudo aptitude install cowsay fortunes-br
echo -e "\nfortune | cowsay -f tux" >> ~/.bashrc
fortune | cowsay -f tux
# Outras opções: apt, beavis.zen, bong, bud-frogs, bunny, cheese, cower, daemon, default, dragon, dragon-and-cow, elephant, elephant-in-snake, eyes, flaming-sheep, ghostbusters, head-in, hellokitty, kiss, kitty, koala, kosh, luke-koala, mech-and-cow, meow, milk, moofasa, moose, mutilated, ren, satanic, sheep, skeleton, small, sodomized, sodomized-sheep, stegosaurus, stimpy, supermilker, surgery, telebears, three-eyes, turkey, turtle, tux, udder, vader, vader-koala, www

echo "*****Remover o Rhythmbox*****"
sudo aptitude remove rhythmbox
echo "*****Remover o Thunderbird*****"
sudo aptitude remove thunderbird

Clean

